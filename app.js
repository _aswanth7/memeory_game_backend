const express = require("express");
const http = require("http");
const bodyParser = require("body-parser");

const config = require("./app/Config/config");
const apiRoute = require("./app/Routes/api");
const db = require("./app/Model/index");

const app = express();
const PORT = config.PORT;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/api", apiRoute);

app.use("*", (req, res, next) => {
  let error = new Error("Not Found");
  error.status = 404;
  next(error);
});

db.sequelize
  .sync()
  .then(() => {
    console.log("Connected to DB");
  })
  .catch((err) => {
    console.log(err);
  });

app.use((error, req, res, next) => {
  let status = error.status || 500;
  console.log(error);
  res.status(status).send(http.STATUS_CODES[status]).end();
});

app.listen(PORT, () => {
  console.log(`Server Started at ${PORT}`);
});
