module.exports = function (sequelize, Sequelize) {
  const Match = sequelize.define("match", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    level: {
      type: Sequelize.INTEGER,
    },
    username: {
      type: Sequelize.STRING,
    },
    usermail: {
      type: Sequelize.STRING,
    },
    score: {
      type: Sequelize.INTEGER,
    },
  });

  return Match;
};
