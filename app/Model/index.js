const Sequelize = require("sequelize");

const config = require("../Config/config");

const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
});

const db = {};

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.match = require("./match")(sequelize, Sequelize);

module.exports = db;
