const db = require("../Model");
const sendMail = require("./mail");

const Match = db.match;
const Op = db.Sequelize.Op;

const create = (req, res, next) => {
  console.log(req.body);
  if (
    !req.body.level ||
    !req.body.username ||
    !req.body.usermail ||
    !req.body.score
  ) {
    res.status(400).json({ error: "Invalid Body" }).end();
  } else {
    let match = {
      level: req.body.level,
      username: req.body.username,
      usermail: req.body.usermail,
      score: req.body.score,
    };
    Match.create(match)
      .then((data) => {
        return sendMail(
          req.body.usermail,
          req.body.username,
          req.body.level,
          req.body.score
        );
      })
      .then((data) => {
        res.status(200).send(data).end();
      })
      .catch((err) => {
        next(err);
        console.log(err);
      });
  }
};

const topscores = (req, res, next) => {
  const level = req.params.level;
  console.log(level);
  const condition = level ? { level: level } : null;
  Match.findAll({
    where: condition,
    order: [["score", "DESC"]],
    limit: 5,
  })
    .then((data) => {
      res.status(200).send(data).end();
    })
    .catch((err) => {
      next(err);
      console.log(err);
    });
};

module.exports = { create, topscores };
