const nodemailer = require("nodemailer");
const config = require("../Config/config");

const sendMail = (to, username, level, score) => {
  const transporter = nodemailer.createTransport({
    host: config.MAIL_HOST,
    port: config.MAIL_PORT,
    auth: {
      user: config.MAIL_USER,
      pass: config.MAIL_PASSWORD,
    },
  });
  const html = `<div style='text-align:center'> <h1 style='color:green'> Congratulation ${username}  </h1><h3>You Won the Memory Game</h3><h4> Level : ${level}</h4><h4> Score : ${score}</h4></div>`;

  const message = {
    from: "score@memory-game-three-beta.vercel.app",
    to,
    subject: "Memory Game Score",
    html,
  };
  return transporter.sendMail(message);
};

module.exports = sendMail;
