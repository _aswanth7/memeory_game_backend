const route = require("express").Router();

const match = require("../Controller/match");

route.get("/topscore/:level", match.topscores);

route.post("/sendmail", match.create);

module.exports = route;
